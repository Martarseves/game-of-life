"use strict;";

// canvas **
let canvas = document.getElementById("gameCanvas");
let ctx = canvas.getContext("2d");

// square dimensions
let squareWidth = 200;
let squareHeight = 100;
let size = 5;
let principalSquare = [];
let secondSquare = [];

// canvas application **
canvas.width = squareWidth * size;
canvas.height = squareHeight * size;
canvas.style.width = canvas.width;
canvas.style.height = canvas.height;

// aleatory initial ecosystem
for (let i = 0; i < squareWidth; i++) {
  principalSquare[i] = [];
  secondSquare[i] = [];
  for (var j = 0; j < squareHeight; j++) {
    principalSquare[i][j] = [];
    secondSquare[i][j] = [];
    let aleatory = Math.random() * 100;
    if (aleatory > 44) {
      principalSquare[i][j] = 1;
    }
  }
}

// square ecosystem
function countNeighbour(i, j) {
  let count = 0;
  function counter(i, j) {
    // if x and y on the grid
    if (i > 0 && i < squareWidth && j > 0 && j < squareHeight) {
      if (principalSquare[i][j] == 1) count++;
    }
  }
  counter(i - 1, j - 1); // 100 000 000
  counter(i - 1, j); // 010 000 000
  counter(i - 1, j + 1); // 001 000 000
  counter(i, j - 1); // 000 100 000
  counter(i, j); // 000 010 000
  counter(i, j + 1); // 000 001 000
  counter(i + 1, j - 1); // 000 000 100
  counter(i + 1, j); // 000 000 010
  counter(i + 1, j + 1); // 000 000 001
  return count;
}

//
function start() {
  for (let i = 0; i < squareWidth; i++) {
    for (let j = 0; j < squareHeight; j++) {
      let count = countNeighbour(i, j);
      if (principalSquare[i][j] == 0) {
        if (count == 3) {
          secondSquare[i][j] = 1;
        }
      } else {
        if (count < 2 || count > 3) {
          secondSquare[i][j] = 0;
        } else {
          secondSquare[i][j] = 1;
        }
      }
    }
  }
  principalSquare = secondSquare;
}

// canvas **
function draw() {
  ctx.fillStyle = "#e2889b";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  for (let i = 0; i < squareWidth; i++) {
    for (let j = 0; j < squareHeight; j++) {
      if (principalSquare[i][j] == 1) {
        ctx.fillStyle = "#e00068";
        ctx.fillRect(i * size, j * size, size, size);
      }
    }
  }
}

function update() {
  start();
  draw();
}

function continuance() {
  let actualtime;
  let lasttime;
  let dt = actualtime - lasttime / 1000;
  lasttime = actualtime;
  setTimeout(continuance, 800);
  update(dt);
}

continuance();
